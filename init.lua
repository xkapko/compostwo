local mod = "ctrl+alt+"
local mod2 = "alt+shift+"

local M = {
	colors = {
		background = { 0.13, 0.13, 0.13, 1.0 },
	},
	keybinds = {},
	tags = {
		"1",
		"2",
		"3",
		"4",
	},
	initial_tag = "1",
}

M.keybinds[mod .. "r"] = "reload"
M.keybinds[mod2 .. "q"] = "quit"
M.keybinds[mod .. "t"] = "run weston-terminal"
M.keybinds[mod .. "f"] = "run "
M.keybinds[mod .. "p"] = "toggle_split"

M.keybinds[mod .. "h"] = "move left"
M.keybinds[mod .. "j"] = "move down"
M.keybinds[mod .. "k"] = "move up"
M.keybinds[mod .. "l"] = "move right"

M.keybinds[mod2 .. "h"] = "shift left"
M.keybinds[mod2 .. "j"] = "shift down"
M.keybinds[mod2 .. "k"] = "shift up"
M.keybinds[mod2 .. "l"] = "shift right"

M.keybinds[mod .. "1"] = "tag 1"
M.keybinds[mod .. "2"] = "tag 2"
M.keybinds[mod .. "3"] = "tag 3"
M.keybinds[mod .. "4"] = "tag 4"

M.keybinds[mod .. "5"] = "client_move 1"
M.keybinds[mod .. "6"] = "client_move 2"
M.keybinds[mod .. "7"] = "client_move 3"
M.keybinds[mod .. "8"] = "client_move 4"

return M
