use anyhow::Result;
use mlua::prelude::*;
use smithay::{
    backend::{renderer::gles::GlesRenderer, winit::WinitGraphicsBackend},
    delegate_data_device, delegate_output, delegate_seat,
    desktop::{PopupManager, Space, Window, WindowSurfaceType},
    input::{Seat, SeatHandler, SeatState},
    reexports::{
        calloop::{generic::Generic, EventLoop, Interest, LoopSignal, Mode, PostAction},
        wayland_server::{
            backend::{ClientData, ClientId, DisconnectReason},
            protocol::wl_surface::WlSurface,
            Display, DisplayHandle, Resource as _,
        },
    },
    utils::{Logical, Point, SERIAL_COUNTER},
    wayland::{
        compositor::{CompositorClientState, CompositorState},
        output::OutputHandler,
        selection::{
            data_device::{
                set_data_device_focus, ClientDndGrabHandler, DataDeviceHandler, DataDeviceState,
                ServerDndGrabHandler,
            },
            SelectionHandler,
        },
        shell::xdg::XdgShellState,
        shm::ShmState,
        socket::ListeningSocketSource,
    },
};

use std::{cell::RefCell, collections::HashMap, ffi::OsString, path::PathBuf, rc::Rc, sync::Arc};

use crate::tree::{BSPTree, Rectangle as Rect};
use crate::CalloopData;
use crate::Config;

#[derive(Debug)]
pub struct State {
    pub start_time: std::time::Instant,
    pub sock: OsString,
    pub display_handle: DisplayHandle,

    // window management
    pub space: Space<Window>,
    pub focused_tag: String,
    pub tags: HashMap<String, BSPTree>,

    pub signal: LoopSignal,
    pub config: Config,
    pub config_path: PathBuf,
    pub lua: Rc<Lua>,
    pub winit: Option<Rc<RefCell<WinitGraphicsBackend<GlesRenderer>>>>,

    // compositor states
    pub seat: Seat<Self>,
    pub xdg_shell_state: XdgShellState,
    pub compositor_state: CompositorState,
    pub shm_state: ShmState,
    //pub output_manager_state: OutputManagerState,
    pub seat_state: SeatState<State>,
    pub data_device_state: DataDeviceState,
    pub popups: PopupManager,
}

impl State {
    pub fn new(
        event_loop: &mut EventLoop<CalloopData>,
        display: Display<Self>,
        cfg_path: Option<String>,
    ) -> Result<Self> {
        let start_time = std::time::Instant::now();

        let dh = display.handle();

        let compositor_state = CompositorState::new::<Self>(&dh);
        let xdg_shell_state = XdgShellState::new::<Self>(&dh);
        let shm_state = ShmState::new::<Self>(&dh, vec![]);
        let mut seat_state = SeatState::new();
        let data_device_state = DataDeviceState::new::<Self>(&dh);
        let popups = PopupManager::default();

        let mut seat: Seat<Self> = seat_state.new_wl_seat(&dh, "winit");
        seat.add_keyboard(Default::default(), 200, 25).unwrap();

        seat.add_pointer();

        let space = Space::default();

        let socket_name = Self::init_wayland_listener(display, event_loop);

        let loop_signal = event_loop.get_signal();

        let lua = Rc::new(Lua::new());
        let config_path = PathBuf::from(cfg_path.unwrap_or(format!(
            "{}/.config/composto/init.lua",
            std::env::var("HOME")?
        )));
        let config = Config::load(&config_path, lua.clone())?;
        let mut tags = HashMap::new();
        config.tags.iter().for_each(|x| {
            tags.insert(x.clone(), BSPTree::new(Rect::new(0, 0, 0, 0)));
        });
        let focused_tag = config.initial_tag.clone();

        Ok(Self {
            lua,
            config,
            config_path,
            start_time,
            display_handle: dh,

            winit: None,
            space,
            signal: loop_signal,
            sock: socket_name,
            focused_tag,
            tags,

            compositor_state,
            xdg_shell_state,
            shm_state,
            // output_manager_state,
            seat_state,
            data_device_state,
            popups,
            seat,
        })
    }

    fn init_wayland_listener(
        display: Display<Self>,
        event_loop: &mut EventLoop<CalloopData>,
    ) -> OsString {
        let listening_socket = ListeningSocketSource::new_auto().unwrap();

        let socket_name = listening_socket.socket_name().to_os_string();

        let handle = event_loop.handle();

        event_loop
            .handle()
            .insert_source(listening_socket, move |client_stream, _, state| {
                state
                    .display_handle
                    .insert_client(client_stream, Arc::new(ClientState::default()))
                    .unwrap();
            })
            .expect("Failed to init the wayland event source.");

        handle
            .insert_source(
                Generic::new(display, Interest::READ, Mode::Level),
                |_, display, state| {
                    unsafe {
                        display
                            .get_mut()
                            .dispatch_clients(&mut state.state)
                            .unwrap();
                    }
                    Ok(PostAction::Continue)
                },
            )
            .unwrap();

        socket_name
    }

    pub fn surface_under(
        &self,
        pos: Point<f64, Logical>,
    ) -> Option<(WlSurface, Point<i32, Logical>)> {
        self.space
            .element_under(pos)
            .and_then(|(window, location)| {
                window
                    .surface_under(pos - location.to_f64(), WindowSurfaceType::ALL)
                    .map(|(s, p)| (s, p + location))
            })
    }

    pub fn render_tree(&mut self, tree: &BSPTree) {
        for node in tree.walk() {
            let rect = node.borrow().get_rect();
            if let Some(window) = node.borrow().get_data() {
                let _ = &window.toplevel().unwrap().with_pending_state(|state| {
                    state.size = Some((rect.w as i32, rect.h as i32).into());
                });
                self.space
                    .map_element(window.clone(), (rect.x as i32, rect.y as i32), false);

                if tree.focused.as_ref().is_some_and(|x| Rc::ptr_eq(&node, x)) {
                    self.set_window_focus(window);
                }
            }
        }
        self.space.refresh();
        self.space.elements().for_each(|window| {
            window.toplevel().unwrap().send_pending_configure();
        });
    }

    pub fn render_tag(&mut self, tag: &impl AsRef<str>) {
        if let Some(tree) = self.tags.get(tag.as_ref()) {
            self.render_tree(&tree.clone());
        }
    }

    pub fn unmap_focused_tag(&mut self) {
        let vec: Vec<Window> = self.space.elements().cloned().collect();
        vec.iter().for_each(|w| self.space.unmap_elem(w));
        self.space.elements().for_each(|window| {
            window.toplevel().unwrap().send_pending_configure();
        });
    }

    pub fn focus_tag(&mut self) {
        self.render_tag(&self.focused_tag.clone());
    }

    pub fn set_window_focus(&mut self, window: &Window) {
        let keyboard = self.seat.get_keyboard().unwrap();
        let serial = SERIAL_COUNTER.next_serial();
        self.space.raise_element(window, true);
        keyboard.set_focus(
            self,
            Some(window.toplevel().unwrap().wl_surface().clone()),
            serial,
        );
        self.space.elements().for_each(|window| {
            window.toplevel().unwrap().send_pending_configure();
        });
    }
}

#[derive(Default)]
pub struct ClientState {
    pub compositor_state: CompositorClientState,
}

impl ClientData for ClientState {
    fn initialized(&self, _client_id: ClientId) {}
    fn disconnected(&self, _client_id: ClientId, _reason: DisconnectReason) {}
}

impl SeatHandler for State {
    type KeyboardFocus = WlSurface;
    type PointerFocus = WlSurface;
    type TouchFocus = WlSurface;

    fn seat_state(&mut self) -> &mut SeatState<State> {
        &mut self.seat_state
    }

    fn cursor_image(
        &mut self,
        _seat: &Seat<Self>,
        _image: smithay::input::pointer::CursorImageStatus,
    ) {
    }

    fn focus_changed(&mut self, seat: &Seat<Self>, focused: Option<&WlSurface>) {
        let dh = &self.display_handle;
        let client = focused.and_then(|s| dh.get_client(s.id()).ok());
        set_data_device_focus(dh, seat, client);
    }
}

delegate_seat!(State);

impl SelectionHandler for State {
    type SelectionUserData = ();
}

impl DataDeviceHandler for State {
    fn data_device_state(&self) -> &DataDeviceState {
        &self.data_device_state
    }
}

impl ClientDndGrabHandler for State {}
impl ServerDndGrabHandler for State {}

delegate_data_device!(State);

//
// Wl Output & Xdg Output
//

impl OutputHandler for State {}
delegate_output!(State);
