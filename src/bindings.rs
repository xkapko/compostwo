use anyhow::Result;
use mlua::{prelude::*, UserData, UserDataFields, UserDataMethods, Value};
use smithay::input::keyboard::{Keysym, ModifiersState};
use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::rc::Rc;

use crate::input::Action;

#[derive(Clone, Debug, Default)]
pub struct Config {
    pub colors: HashMap<String, [f32; 4]>,
    pub keybinds: HashMap<String, String>,
    pub tags: HashSet<String>,
    pub initial_tag: String,
}

#[derive(Clone, Debug)]
pub struct Keybind {
    pub modifiers: ModifiersState,
    pub syms: Vec<Keysym>,
    pub action: String,
}

impl Keybind {
    pub fn action(&self) -> Option<Action> {
        if self.action.starts_with("quit") {
            return Some(Action::Quit);
        } else if self.action.starts_with("reload") {
            return Some(Action::Config);
        } else if self.action.starts_with("run") {
            let stripped = self.action.strip_prefix("run ");
            if let Some(s) = stripped {
                return Some(Action::Run(s.to_string()));
            }
            return None;
        } else if self.action == "toggle_split" {
            return Some(Action::ToggleFocus);
        } else if self.action.starts_with("move") {
            if let Some(rest) = self.action.strip_prefix("move ") {
                return match rest {
                    "left" => Some(Action::ShiftFocus(crate::tree::MoveDirection::Left)),
                    "right" => Some(Action::ShiftFocus(crate::tree::MoveDirection::Right)),
                    "up" => Some(Action::ShiftFocus(crate::tree::MoveDirection::Up)),
                    "down" => Some(Action::ShiftFocus(crate::tree::MoveDirection::Down)),
                    _ => None,
                };
            }
        } else if self.action.starts_with("tag") {
            if let Some(rest) = self.action.strip_prefix("tag ") {
                return Some(Action::SwitchToTag(rest.to_owned()));
            }
        } else if self.action.starts_with("shift") {
            if let Some(rest) = self.action.strip_prefix("shift ") {
                return match rest {
                    "left" => Some(Action::MoveClient(crate::tree::MoveDirection::Left)),
                    "right" => Some(Action::MoveClient(crate::tree::MoveDirection::Right)),
                    "up" => Some(Action::MoveClient(crate::tree::MoveDirection::Up)),
                    "down" => Some(Action::MoveClient(crate::tree::MoveDirection::Down)),
                    _ => None,
                };
            }
        } else if self.action.starts_with("client_move") {
            if let Some(rest) = self.action.strip_prefix("client_move ") {
                return Some(Action::TagClient(rest.to_owned()));
            }
        } else if self.action == "kill" {
            return Some(Action::Kill);
        }
        None
    }
}

impl<'lua> FromLua<'lua> for Config {
    fn from_lua(value: Value<'lua>, _: &'lua Lua) -> LuaResult<Self> {
        match value {
            Value::Table(t) => Ok(Config {
                colors: t.get("colors")?,
                keybinds: t.get("keybinds")?,
                tags: t.get("tags")?,
                initial_tag: t.get("initial_tag")?,
            }),
            _ => Err(LuaError::runtime("unable to parse value into config")),
        }
    }
}

impl UserData for Config {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(_: &mut F) {}

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(_: &mut M) {}
}

impl Config {
    pub fn load(path: &impl AsRef<Path>, lua: Rc<Lua>) -> Result<Self> {
        let path = path.as_ref();
        let chunk = lua.load(path);

        Ok(chunk.eval()?)
    }

    pub fn process_keybinds(&self) -> Vec<Keybind> {
        let mut out = Vec::new();
        for (keys, action) in self.keybinds.iter() {
            let mut mods = ModifiersState::default();
            let mut syms = Vec::new();

            for key in keys.split('+') {
                match key {
                    "ctrl" => mods.ctrl = true,
                    "alt" => mods.alt = true,
                    "shift" => mods.shift = true,
                    "caps_lock" => mods.caps_lock = true,
                    "logo" => mods.logo = true,
                    "num_lock" => mods.num_lock = true,
                    "iso_level3_shift" => mods.iso_level3_shift = true,
                    _ => syms.push(Keysym::from_char(key.chars().next().unwrap())),
                }
            }

            out.push(Keybind {
                modifiers: mods,
                syms,
                action: action.to_string(),
            })
        }

        out
    }
}
