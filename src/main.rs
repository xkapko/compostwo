mod bindings;
mod errors;
mod handlers;
mod input;
mod state;
mod tree;

use anyhow::Result;
use bindings::Config;
use smithay::{
    backend::{
        renderer::{
            damage::OutputDamageTracker, element::surface::WaylandSurfaceRenderElement,
            gles::GlesRenderer,
        },
        winit::{init, WinitEvent},
    },
    output::{Mode, Output, Subpixel},
    reexports::{
        calloop::EventLoop,
        wayland_server::{Display, DisplayHandle},
    },
    utils::{Rectangle, Transform},
};
use state::State;
use std::{cell::RefCell, time::Duration};
use std::{
    env::{args, set_var},
    rc::Rc,
};

pub struct CalloopData {
    state: State,
    display_handle: DisplayHandle,
}

fn main() -> Result<()> {
    let mut event_loop: EventLoop<CalloopData> = EventLoop::try_new()?;

    let display: Display<State> = Display::new()?;
    let display_handle = display.handle();
    let conf = args().skip(1).take(1).next();
    let state = State::new(&mut event_loop, display, conf)?;

    let mut data = CalloopData {
        state,
        display_handle,
    };

    init_winit(&mut event_loop, &mut data)?;

    event_loop.run(None, &mut data, move |_| {})?;

    Ok(())
}

pub fn init_winit(event_loop: &mut EventLoop<CalloopData>, data: &mut CalloopData) -> Result<()> {
    let display_handle = &mut data.display_handle;
    let state = &mut data.state;

    let (backend, winit) = init().expect("failed to init winit");
    let backend = Rc::new(RefCell::new(backend));
    state.winit = Some(backend.clone());

    let mode = Mode {
        size: backend.borrow().window_size(),
        refresh: 60_000,
    };

    let output = Output::new(
        "winit".to_string(),
        smithay::output::PhysicalProperties {
            size: (0, 0).into(),
            subpixel: Subpixel::Unknown,
            make: "composto".into(),
            model: "Winit".into(),
        },
    );
    let _global = output.create_global::<State>(display_handle);
    output.change_current_state(
        Some(mode),
        Some(Transform::Flipped180),
        None,
        Some((0, 0).into()),
    );
    output.set_preferred(mode);

    state.space.map_output(&output, (0, 0));

    let mut damage_tracker = OutputDamageTracker::from_output(&output);

    set_var("WAYLAND_DISPLAY", &state.sock);

    event_loop
        .handle()
        .insert_source(winit, move |event, _, data| {
            let display = &mut data.display_handle;
            let state = &mut data.state;

            match event {
                WinitEvent::Resized { size, .. } => {
                    output.change_current_state(
                        Some(Mode {
                            size,
                            refresh: 60_000,
                        }),
                        None,
                        None,
                        None,
                    );
                    state
                        .tags
                        .iter_mut()
                        .for_each(|(_, y)| y.resize((size.w, size.h)));
                    backend.borrow().window().request_redraw();
                }
                WinitEvent::Input(event) => {
                    state.process_input(event).unwrap_or(());
                    backend.borrow().window().request_redraw();
                }
                WinitEvent::Redraw => {
                    let size = backend.borrow().window_size();
                    let damage = Rectangle::from_loc_and_size((0, 0), size);

                    backend.borrow_mut().bind().unwrap();
                    state.render_tag(&state.focused_tag.clone());
                    smithay::desktop::space::render_output::<
                        _,
                        WaylandSurfaceRenderElement<GlesRenderer>,
                        _,
                        _,
                    >(
                        &output,
                        backend.borrow_mut().renderer(),
                        1.0,
                        0,
                        [&state.space],
                        &[],
                        &mut damage_tracker,
                        state.config.colors["background"],
                    )
                    .unwrap();
                    backend.borrow_mut().submit(Some(&[damage])).unwrap();

                    state.space.elements().for_each(|window| {
                        window.send_frame(
                            &output,
                            state.start_time.elapsed(),
                            Some(Duration::ZERO),
                            |_, _| Some(output.clone()),
                        )
                    });

                    state.space.refresh();
                    state.popups.cleanup();
                    let _ = display.flush_clients();

                    // Ask for redraw to schedule new frame.
                    backend.borrow().window().request_redraw();
                }
                WinitEvent::CloseRequested => {
                    state.signal.stop();
                }
                _ => (),
            };
        })
        .unwrap();

    Ok(())
}
