use anyhow::Result;
use smithay::backend::input::{
    AbsolutePositionEvent, Axis, AxisSource, ButtonState, Event, InputBackend, InputEvent,
    KeyState, KeyboardKeyEvent, PointerAxisEvent, PointerButtonEvent,
};
use smithay::input::{
    keyboard::FilterResult,
    pointer::{AxisFrame, ButtonEvent, MotionEvent},
};
use smithay::reexports::wayland_server::protocol::wl_surface::WlSurface;
use smithay::utils::SERIAL_COUNTER;
use std::process::Command;

use crate::tree::MoveDirection;
use crate::{Config, State};

pub enum Action {
    Quit,
    Config,
    Kill,
    Run(String),
    ToggleFocus,
    ShiftFocus(MoveDirection),
    TagClient(String),
    MoveClient(MoveDirection),
    SwitchToTag(String),
}

impl State {
    pub fn process_input<I: InputBackend>(&mut self, event: InputEvent<I>) -> Result<()> {
        match event {
            InputEvent::Keyboard { event, .. } => {
                let serial = SERIAL_COUNTER.next_serial();
                let time = Event::time_msec(&event);
                let state = event.state();

                let keybinds = self.config.process_keybinds();

                let res = self.seat.get_keyboard().unwrap().input::<Action, _>(
                    self,
                    event.key_code(),
                    event.state(),
                    serial,
                    time,
                    |_, modifiers, handle| {
                        let keysym = handle.modified_sym();

                        if state == KeyState::Pressed {
                            for bind in keybinds.iter() {
                                let mut b = bind.clone();
                                b.modifiers.serialized = modifiers.serialized;
                                if b.modifiers == *modifiers
                                    && keysym.key_char().map(|c| c.to_ascii_lowercase())
                                        == b.syms[0].key_char().map(|c| c.to_ascii_lowercase())
                                {
                                    let a = bind.action();
                                    if let Some(action) = a {
                                        return FilterResult::Intercept(action);
                                    }
                                    return FilterResult::Forward;
                                }
                            }
                            FilterResult::Forward
                        } else {
                            FilterResult::Forward
                        }
                    },
                );
                if let Some(action) = res {
                    self.handle_action(action)?;
                }
            }
            InputEvent::PointerMotionAbsolute { event } => {
                let output = self.space.outputs().next().unwrap();

                let output_geo = self.space.output_geometry(output).unwrap();

                let pos = event.position_transformed(output_geo.size) + output_geo.loc.to_f64();

                let serial = SERIAL_COUNTER.next_serial();

                let pointer = self.seat.get_pointer().unwrap();
                let keyboard = self.seat.get_keyboard().unwrap();

                let under = self.surface_under(pos);
                self.tags
                    .entry(self.focused_tag.clone())
                    .and_modify(|t| t.focus_coords(pos.x as i32, pos.y as i32));

                if !pointer.is_grabbed() {
                    if let Some((window, _loc)) = self
                        .space
                        .element_under(pointer.current_location())
                        .map(|(w, l)| (w.clone(), l))
                    {
                        self.space.raise_element(&window, true);
                        keyboard.set_focus(
                            self,
                            Some(window.toplevel().unwrap().wl_surface().clone()),
                            serial,
                        );
                        self.space.elements().for_each(|window| {
                            window.toplevel().unwrap().send_pending_configure();
                        });
                    } else {
                        self.space.elements().for_each(|window| {
                            window.set_activated(false);
                            window.toplevel().unwrap().send_pending_configure();
                        });
                        keyboard.set_focus(self, Option::<WlSurface>::None, serial);
                    }
                }

                pointer.motion(
                    self,
                    under,
                    &MotionEvent {
                        location: pos,
                        serial,
                        time: event.time_msec(),
                    },
                );
                pointer.frame(self);
            }
            InputEvent::PointerButton { event } => {
                let pointer = self.seat.get_pointer().unwrap();
                let keyboard = self.seat.get_keyboard().unwrap();

                let serial = SERIAL_COUNTER.next_serial();

                let button = event.button_code();

                let button_state = event.state();

                if ButtonState::Pressed == button_state && !pointer.is_grabbed() {
                    if let Some((window, _loc)) = self
                        .space
                        .element_under(pointer.current_location())
                        .map(|(w, l)| (w.clone(), l))
                    {
                        self.space.raise_element(&window, true);
                        keyboard.set_focus(
                            self,
                            Some(window.toplevel().unwrap().wl_surface().clone()),
                            serial,
                        );
                        self.space.elements().for_each(|window| {
                            window.toplevel().unwrap().send_pending_configure();
                        });
                    } else {
                        self.space.elements().for_each(|window| {
                            window.set_activated(false);
                            window.toplevel().unwrap().send_pending_configure();
                        });
                        keyboard.set_focus(self, Option::<WlSurface>::None, serial);
                    }
                };

                pointer.button(
                    self,
                    &ButtonEvent {
                        button,
                        state: button_state,
                        serial,
                        time: event.time_msec(),
                    },
                );
                pointer.frame(self);
            }
            InputEvent::PointerAxis { event, .. } => {
                let source = event.source();

                let horizontal_amount = event.amount(Axis::Horizontal).unwrap_or_else(|| {
                    event.amount_v120(Axis::Horizontal).unwrap_or(0.0) * 15.0 / 120.
                });
                let vertical_amount = event.amount(Axis::Vertical).unwrap_or_else(|| {
                    event.amount_v120(Axis::Vertical).unwrap_or(0.0) * 15.0 / 120.
                });
                let horizontal_amount_discrete = event.amount_v120(Axis::Horizontal);
                let vertical_amount_discrete = event.amount_v120(Axis::Vertical);

                let mut frame = AxisFrame::new(event.time_msec()).source(source);
                if horizontal_amount != 0.0 {
                    frame = frame.value(Axis::Horizontal, horizontal_amount);
                    if let Some(discrete) = horizontal_amount_discrete {
                        frame = frame.v120(Axis::Horizontal, discrete as i32);
                    }
                }
                if vertical_amount != 0.0 {
                    frame = frame.value(Axis::Vertical, vertical_amount);
                    if let Some(discrete) = vertical_amount_discrete {
                        frame = frame.v120(Axis::Vertical, discrete as i32);
                    }
                }

                if source == AxisSource::Finger {
                    if event.amount(Axis::Horizontal) == Some(0.0) {
                        frame = frame.stop(Axis::Horizontal);
                    }
                    if event.amount(Axis::Vertical) == Some(0.0) {
                        frame = frame.stop(Axis::Vertical);
                    }
                }

                let pointer = self.seat.get_pointer().unwrap();
                pointer.axis(self, frame);
                pointer.frame(self);
            }
            _ => {}
        }
        Ok(())
    }
    pub fn handle_action(&mut self, action: Action) -> Result<()> {
        match action {
            Action::Config => {
                self.config = Config::load(&self.config_path, self.lua.clone())?;
            }
            Action::Run(cmd) => {
                let _ = Command::new(cmd)
                    .env("WAYLAND_DISPLAY", self.sock.clone())
                    .spawn();
            }
            Action::ToggleFocus => {
                self.tags
                    .entry(self.focused_tag.clone())
                    .and_modify(|x| x.toggle_split());
            }
            Action::ShiftFocus(dir) => {
                self.tags
                    .get_mut(&self.focused_tag)
                    .expect("should not happen")
                    .move_focus(dir);

                if let Some(t) = self.tags.get(&self.focused_tag).as_ref() {
                    if let Some(node) = t.focused.as_ref() {
                        let id = node.borrow().get_data().unwrap().clone();

                        let win = self
                            .space
                            .elements()
                            .find(|x| {
                                x.toplevel()
                                    .is_some_and(|top| top == id.toplevel().unwrap())
                            })
                            .cloned();

                        if let Some(win) = win {
                            self.set_window_focus(&win);
                        }
                    }
                }
            }
            Action::TagClient(tag) => {
                let mut data = None;
                self.tags
                    .entry(self.focused_tag.clone())
                    .and_modify(|tree| {
                        if let Some(node) = tree.focused.as_ref() {
                            data = node.borrow().get_data().cloned();
                        }
                        tree.delete_focused();
                    });
                self.tags.entry(tag).and_modify(|tree| {
                    if let Some(data) = data {
                        tree.insert(data);
                    }
                });
                self.unmap_focused_tag();
                self.focus_tag();
            }
            Action::MoveClient(dir) => {
                self.tags
                    .entry(self.focused_tag.clone())
                    .and_modify(|t| t.move_node(dir).unwrap_or(()));
                self.focus_tag();
            }
            Action::SwitchToTag(tag) => {
                self.unmap_focused_tag();
                self.focused_tag = tag.clone();
                self.focus_tag();
            }
            Action::Kill => {
                self.tags.entry(self.focused_tag.clone()).and_modify(|t| {
                    if let Some(focused) = t.focused.as_ref() {
                        if let Some(toplevel) = focused
                            .borrow()
                            .get_data()
                            .expect("Missing node data")
                            .toplevel()
                        {
                            toplevel.send_close();
                        }
                    }
                });
            }
            Action::Quit => self.signal.stop(),
        }

        self.winit
            .as_ref()
            .unwrap()
            .borrow()
            .window()
            .request_redraw();

        Ok(())
    }
}
